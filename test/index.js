(function () {
    'use strict';

    var Lab = require('lab');
    var Code = require('code');
    var expect = Code.expect;
    var lab = exports.lab = Lab.script();

    var internals = {};
    var before = lab.before;
    var after = lab.after;
    var describe = lab.experiment;
    var it = lab.test;

    describe('FTPLST', function () {

        it('can be required', function (done) {

            var FTPLST = require('..');
            expect(FTPLST).to.exist;
            done();

        });
    });
})();
