/**
 * Created by flexalex on 3/10/15.
 */

(function () {
    'use strict';

    var gulp = require('gulp');
    var del = require('del');
    var babel = require('gulp-babel');
    var jshint = require('gulp-jshint');
    var jscs = require('gulp-jscs-custom');
    var lab = require('gulp-lab');
    var fs = require('fs');

    var dest = 'build';
    var paths = {
        lib: ['lib/**/*.js'],
        tests: ['test/**/*.js']
    };

    /***
     * Clean the build folder
     */
    gulp.task('clean', function (cb) {
        del([dest], cb);
    });

    /***
     * Run babel transpiler
     */
    gulp.task('run', ['clean', 'lint', 'jscs'], function () {
        gulp
            .src(paths.tests)
            .pipe(babel())
            .pipe(gulp.dest(dest + '/test'));

        gulp
            .src(paths.lib)
            .pipe(babel())
            .pipe(gulp.dest(dest + '/lib'));

        return gulp.src('./index.js')
            .pipe(gulp.dest(dest));
    });
    /***
     * Run jscs
     */
    gulp.task('jscs', function () {
        return gulp.src(paths.lib)
            .pipe(jscs({
                esnext: true,
                configPath: '.jscsrc',
                reporter: 'console'
            }));
    });

    /***
     * Run js hint
     */
    gulp.task('lint', function () {
        return gulp.src(paths.lib)
            .pipe(jshint())
            .pipe(jshint.reporter('jshint-stylish'))
            /*.pipe(jshint.reporter('fail'))*/;
    });

    /***
     * Watcher for all the tasks
     */
    gulp.task('watch', [
        'reload'
    ], function () {
        gulp.watch(paths.tests, [
            'reload'
        ]);
        gulp.watch(paths.lib, [
            'reload'
        ]);
    });

    /***
     * Run the testing
     * */
    gulp.task('test', ['run'], function () {
        return gulp.src(dest + '/test/**/*.js')
            .pipe(lab({
                args: '-v -l -C',
                opts: {
                    emitLabError: true
                }
            }));
    });

    /**
     * Write the reload file to
     */
    gulp.task('reload', ['test'], function () {
        fs.writeFile('build/reloaded.txt', new Date());
    });

    /***
     * default task
     */
    gulp.task('default', [
        'reload'
    ]);

})();
